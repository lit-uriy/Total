@echo off
SETLOCAL

set Formatter=ProjectFormatter\FormatProjectSources.py
set ComplexRoot=..\..\..

%Formatter% %ComplexRoot% --check

ENDLOCAL
