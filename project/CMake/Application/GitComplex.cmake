# Copyright (c) 2018-2019 Viktor Kireev
# Distributed under the MIT License

set(GitComplexApplicationPath ${CMAKE_CURRENT_SOURCE_DIR}/application)

add_subdirectory(${GitComplexApplicationPath}/GitComplex application/GitComplex)
