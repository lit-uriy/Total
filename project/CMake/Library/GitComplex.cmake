# Copyright (c) 2018-2019 Viktor Kireev
# Distributed under the MIT License

set(GitComplexLibraryPath ${CMAKE_CURRENT_SOURCE_DIR}/library/GitComplex)

add_subdirectory(${GitComplexLibraryPath}/Domain/Generic library/GitComplex/Domain/Generic)
add_subdirectory(${GitComplexLibraryPath}/Domain/Std library/GitComplex/Domain/Std)
add_subdirectory(${GitComplexLibraryPath}/Domain/Qt library/GitComplex/Domain/Qt)
add_subdirectory(${GitComplexLibraryPath}/Presentation library/GitComplex/Presentation)
add_subdirectory(${GitComplexLibraryPath}/Assembly library/GitComplex/Assembly)
