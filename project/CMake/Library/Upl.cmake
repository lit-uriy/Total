# Copyright (c) 2018-2019 Viktor Kireev
# Distributed under the MIT License

set(UplLibraryPath ${CMAKE_CURRENT_SOURCE_DIR}/library/UPL)

add_subdirectory(${UplLibraryPath}/project/CMake library/UPL)
